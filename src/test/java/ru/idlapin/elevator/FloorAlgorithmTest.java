package ru.idlapin.elevator;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.TreeSet;

public class FloorAlgorithmTest {

    @Test
    public void algoTest1() {
        FloorAlgorithm floorAlgorithm = new FloorAlgorithm(1, Direction.NONE, Arrays.asList(5, 6, 2));

        TreeSet<Integer> expectedResult = new TreeSet<>();
        expectedResult.addAll(Arrays.asList(2, 5 ,6));
        Assert.assertEquals(expectedResult, floorAlgorithm.getFloorsSequence());
    }

    @Test
    public void algoTest2() {
        FloorAlgorithm floorAlgorithm = new FloorAlgorithm(5, Direction.DOWN, Arrays.asList(6, 1, 3));

        TreeSet<Integer> expectedResult = new TreeSet<>();
        expectedResult.addAll(Arrays.asList(6, 3 ,1));
        Assert.assertEquals(expectedResult, floorAlgorithm.getFloorsSequence());
    }

    @Test
    public void algoTest3() {
        FloorAlgorithm floorAlgorithm = new FloorAlgorithm(5, Direction.DOWN, Arrays.asList(6, 1, 3, 4));

        TreeSet<Integer> expectedResult = new TreeSet<>();
        expectedResult.addAll(Arrays.asList(4, 3 ,1, 6));
        Assert.assertEquals(expectedResult, floorAlgorithm.getFloorsSequence());
    }

    @Test
    public void algoTest4() {
        FloorAlgorithm floorAlgorithm = new FloorAlgorithm(5, Direction.UP, Arrays.asList(6, 1, 3, 4));

        TreeSet<Integer> expectedResult = new TreeSet<>();
        expectedResult.addAll(Arrays.asList(6, 4 ,3, 1));
        Assert.assertEquals(expectedResult, floorAlgorithm.getFloorsSequence());
    }

    @Test
    public void algoTest5() {
        FloorAlgorithm floorAlgorithm = new FloorAlgorithm(5, Direction.UP, Arrays.asList(6, 5, 1, 3, 4));

        TreeSet<Integer> expectedResult = new TreeSet<>();
        expectedResult.addAll(Arrays.asList(5, 6, 4 ,3, 1));
        Assert.assertEquals(expectedResult, floorAlgorithm.getFloorsSequence());
    }
}