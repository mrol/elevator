package ru.idlapin.elevator;

public enum Direction {
    UP(1),
    DOWN(-1),
    NONE(0);

    private Integer factor;

    Direction(Integer factor) {
        this.factor = factor;
    }

    public int getFactor() {
        return factor;
    }
}
