package ru.idlapin.elevator;

class MovingEngine {

    private final Double speed;
    private final Double floorHeight;
    private final Double doorsOpeningTime;

    private Direction direction = Direction.NONE;
    private Integer currentFloor = 1;
    private Double currentPosition = 0d;
    private Integer destinationFloor = 1;

    /**
     * Длительность одного тика в миллисекундах
     */
    private final int TICK_DURATION = 100;

    private final double DELTA = 0.01;

    MovingEngine(Double speed,
                 Double floorHeight,
                 Double doorsOpeningTime) {

        this.speed = speed;
        this.floorHeight = floorHeight;
        this.doorsOpeningTime = doorsOpeningTime;
    }

    public void setDestination(Integer floor) {
        this.destinationFloor = floor;
    }

    public void move() throws InterruptedException {
        direction = calculateDirection(currentFloor, destinationFloor);

        if (Direction.NONE.equals(direction)) {
            return;
        }

        while (!destinationFloor.equals(currentFloor)) {
            currentPosition = Util.round(
                    currentPosition + direction.getFactor() * speed * TICK_DURATION / 1000
                    , 2);
            Thread.sleep(TICK_DURATION);
//            System.out.println("Position = " + currentPosition);
            Integer currentFloor = calculateCurrentFloor();
            if (currentFloor != null) {
                this.currentFloor = currentFloor;
                onCurrentFloorChanged();
                direction = calculateDirection(currentFloor, destinationFloor);
            }
        }

        this.direction = Direction.NONE;
    }

    protected void onCurrentFloorChanged() {
        System.out.println("Current floor is " + currentFloor);
        direction = calculateDirection(currentFloor, destinationFloor);
    }


    public Integer getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Расчитываем текущий этаж
     *
     * @return возвращает текущий этаж. Если лифт находится между этажами, возвращаем null
     */
    private Integer calculateCurrentFloor() {
        if (currentPosition % floorHeight < DELTA) {
            return (int) (currentPosition / floorHeight + 1);
        }
        return null;
    }

    private Direction calculateDirection(Integer floorFrom, Integer floorTo) {
        if (floorFrom.equals(floorTo)) {
            return Direction.NONE;
        }
        return (floorTo > floorFrom) ? Direction.UP : Direction.DOWN;
    }

    public void openAndCloseDoors() throws InterruptedException {
        System.out.println("The Doors are open");
        Thread.sleep(Math.round(doorsOpeningTime * 1000));
        System.out.println("The Doors are closed");
    }

    public Direction getDirection() {
        return direction;
    }
}
