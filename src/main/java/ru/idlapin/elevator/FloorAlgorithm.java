package ru.idlapin.elevator;

import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;

public class FloorAlgorithm {
    private Integer currentFloor;
    private Direction direction;

    private TreeSet<Integer> floorsSequence;

    public FloorAlgorithm(Integer currentFloor, Direction direction, Collection<Integer> floorsSequence) {
        this.currentFloor = currentFloor;
        this.direction = direction;
        this.floorsSequence = new TreeSet<>(new FloorsComparator());
        this.floorsSequence.addAll(floorsSequence);
    }

    public TreeSet<Integer> getFloorsSequence() {
        return floorsSequence;
    }

    private class FloorsComparator implements Comparator<Integer> {

        public int compare(Integer o1, Integer o2) {
            if (o1.equals(o2)) {
                return 0;
            }
            Integer delta1 = currentFloor - o1;
            Integer delta2 = currentFloor - o2;
            if (Math.abs(delta1) > Math.abs(delta2)) {
                return 1;
            } else if (Math.abs(delta1) < Math.abs(delta2)) {
                return -1;
            } else if (Direction.UP.equals(direction)) {
                if (delta1 < 0) {
                    return -1;
                } else return 1;
            } else if (Direction.DOWN.equals(direction)) {
                if (delta1 > 0) {
                    return 1;
                } else {
                    return -1;
                }
            } else if (Direction.NONE.equals(direction)) {
                return 1;
            }
            return 0;
        }
    }
}
