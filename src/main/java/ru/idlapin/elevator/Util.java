package ru.idlapin.elevator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Util {

    /**
     * Утилитный класс округления
     * @param value число, которое обновляем
     * @param places количество знаков после запятой
     * @return округленное число
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
