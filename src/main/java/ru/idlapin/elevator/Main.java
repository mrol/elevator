package ru.idlapin.elevator;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Integer numberOfFloors;
        try {
            numberOfFloors = Integer.valueOf(args[0]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Number of floors must be integer");
        }
        Double floorHeight;
        try {
            floorHeight = Double.valueOf(args[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Floor height must be double");
        }
        Double speed;
        try {
            speed = Double.valueOf(args[2]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Speed must be double");
        }
        Double doorsOpeningTime;
        try {
            doorsOpeningTime = Double.valueOf(args[3]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Doors opening time must be double");
        }

        Elevator elevator = new Elevator(
                speed,
                numberOfFloors,
                floorHeight,
                doorsOpeningTime
        );

        Scanner scnr = new Scanner(System.in);
        System.out.println("Enter the floor:");

        while (true) {
            elevator.addStop(scnr.nextInt());
        }
    }
}
