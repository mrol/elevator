package ru.idlapin.elevator;

import java.util.HashSet;
import java.util.Set;

public class Elevator {


    private FloorAlgorithm floorAlgorithm;
    private Set<Integer> stopsBuffer;
    private MovingEngine movingEngine;
    private Integer numberOfFloors;

    public Elevator(Double speed, Integer numberOfFloors, Double floorHeight, Double doorsOpeningTime) {
        if (numberOfFloors < 5 || numberOfFloors > 20) {
            throw new IllegalArgumentException("The number of floors must be ge than 5 and le than 20");
        }

        if (speed <= 0) {
            throw new IllegalArgumentException("The speed must be greater than 0");
        }
        this.numberOfFloors = numberOfFloors;
        stopsBuffer = new HashSet<>();

        movingEngine = new MovingEngine(speed, floorHeight, doorsOpeningTime) {
            @Override
            protected void onCurrentFloorChanged() {
                super.onCurrentFloorChanged();

                rebuildAlgorithm();
            }
        };

        System.out.println("Current floor is " + movingEngine.getCurrentFloor());
    }

    private void rebuildAlgorithm() {
        if (floorAlgorithm != null) {
            stopsBuffer.addAll(floorAlgorithm.getFloorsSequence());
        }

        floorAlgorithm = new FloorAlgorithm(
                movingEngine.getCurrentFloor(),
                movingEngine.getDirection(),
                stopsBuffer
        );
        stopsBuffer.clear();

        Integer currentFloor = movingEngine.getCurrentFloor();
        if (currentFloor != null && floorAlgorithm.getFloorsSequence().first().equals(currentFloor)) {
            try {
                movingEngine.openAndCloseDoors();
            } catch (InterruptedException e) {
                System.out.println("Doors are stuck.");
                e.printStackTrace();
            }
            floorAlgorithm.getFloorsSequence().pollFirst();
        }

        if (!floorAlgorithm.getFloorsSequence().isEmpty()) {
            movingEngine.setDestination(floorAlgorithm.getFloorsSequence().first());
        }
    }

    public synchronized void addStop(Integer floorToStop) {
        if (floorToStop > numberOfFloors || floorToStop < 1) {
            System.out.println("Destination flor must be ge than 1 and le than " + numberOfFloors);
            return;
        }
        stopsBuffer.add(floorToStop);
        if (Direction.NONE.equals(movingEngine.getDirection())) {
            rebuildAlgorithm();
            Thread thread = new Thread(() -> {
                try {
                    movingEngine.move();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }

    }


}
